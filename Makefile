#
# Makefile romp - a STOMP server written in rust
#

SRC=$(wildcard src/*.rs src/*/*.rs src/*/*/*.rs)

target/release/romp: $(SRC)
	cargo build --release


.PHONY: clean install uninstall run test deb

run:
	cargo run

test:
	cargo test

deb:
	sudo deploy/build-deb.sh

clean:
	cargo clean
	rm -f target/
	mkdir target

install:
	sudo dpkg --install target/romp-*.deb

uninstall:
	sudo dpkg --remove target/romp-*.deb

