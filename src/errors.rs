//! Common error definitions returned by the `romp` server.

/// Errors that the server can generate
#[derive(Debug, PartialEq)]
pub enum ServerError {
    BufferFlup,
    Io,
    General,
    DestinationUnknown,
    DestinationFlup,
    MessageFlup,
    SubscriptionFlup,
    ShuttingDown,
}

/// Errors that the client can generate
#[derive(Debug, PartialEq)]
pub enum ClientError {
    Syntax,
    Timeout,
    HdrFlup,
    BodyFlup,
    SubsFlup,
}
