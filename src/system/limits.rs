use libc::rlimit;
use libc::setrlimit;
use libc::RLIMIT_NOFILE;

/// Set system limits on number of "open files", two files are used
/// per TCP socket in Linux
pub fn increase_rlimit_nofile(limit: u64) -> i32 {
    let lim = rlimit {
        rlim_cur: limit,
        rlim_max: limit,
    };
    unsafe {
        return setrlimit(RLIMIT_NOFILE, &lim);
    }
}
