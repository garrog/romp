use crate::prelude::*;

use crate::message::response::get_response_receipt;

/// handles any message that requested a receipt
///
#[derive(Debug)]
pub struct ReceiptFilter {}

impl ReceiptFilter {
    pub fn new() -> ReceiptFilter {
        ReceiptFilter {}
    }
}

impl MessageFilter for ReceiptFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match message.command {
            StompCommand::Send
            | StompCommand::Abort
            | StompCommand::Subscribe
            | StompCommand::Commit
            | StompCommand::Begin
            | StompCommand::Disconnect
            | StompCommand::Unsubscribe => {
                if let Some(hdr) = message.get_header("receipt") {
                    if context.attributes.get("cancel-receipt").is_some() {
                        return Ok(CONTINUE);
                    }
                    context
                        .session
                        .read()
                        .unwrap()
                        .send_message(get_response_receipt(hdr));
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}
