use log::*;

use crate::prelude::*;

use crate::persist::mlog::{MLog, SyncMLog};
use std::fmt;

/// Persists a copy of every SEND message received to /var/log/romp.mlog
/// N.B server generated messages are not stored.
/// This filter will likely kill the performance of the server since writing is synchronized.
/// Lock per destination might be more acceptable

pub struct PersistFilter {
    mlog: SyncMLog,
}

impl PersistFilter {
    pub fn new() -> PersistFilter {
        if let Ok(mlog) = SyncMLog::new("/var/log/romp.mlog") {
            return PersistFilter { mlog };
        } else {
            panic!("could not open /var/log/romp.mlog");
        }
    }
}

impl MessageFilter for PersistFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match message.command {
            StompCommand::Send => {
                let mut user_str = "anon";
                let session = context.session.read().unwrap();
                // how to borrow an Option<String>
                let user = &session.user.as_ref();
                if user.is_some() {
                    user_str = user.unwrap().as_str()
                }
                match self.mlog.store(user_str, message) {
                    Ok(_) => {}
                    Err(e) => {
                        warn!("persisting failed {}", e);
                        // some use cases may want to panic
                    }
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}

impl fmt::Debug for PersistFilter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PersistFilter")
    }
}
