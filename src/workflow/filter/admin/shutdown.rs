//! Handles SEND to `/virt/admin/shutdown` and `/virt/admin/kill`.

use crate::init::SERVER;
use crate::message::stomp_message::StompCommand;
use crate::message::stomp_message::StompMessage;
use crate::util::some_string;
use crate::workflow::context::Context;
use crate::workflow::filter::{AdminFilter, FilterError};
use crate::workflow::CONTINUE;

pub struct ShutdownFilter {}

impl ShutdownFilter {
    pub fn new() -> ShutdownFilter {
        ShutdownFilter {}
    }
}

impl AdminFilter for ShutdownFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        _context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        if StompCommand::Send == message.command {
            if let Some(destination_name) = message.get_header("destination") {
                if "/virt/admin/shutdown".eq(destination_name) {
                    SERVER.shutdown();
                    return Err(FilterError {
                        reply_message: None,
                        log_message: some_string("shutdown requested"),
                        fatal: true,
                    });
                } else if "/virt/admin/kill".eq(destination_name) {
                    std::process::exit(99);
                }
            }
        }

        Ok(CONTINUE)
    }
}
