use std::sync::Arc;

use crate::prelude::*;

use crate::message::response::SERVER_TAG;
use crate::message::stomp_message::{Header, Ownership};
use crate::util::some_string;

/// handles the MESSAGE STOMP messages type from upstream servers, by echoing them back.
/// serves no purpose other than to test upstream/downstream
#[derive(Debug)]
pub struct EchoFilter {}

impl EchoFilter {
    pub fn new() -> EchoFilter {
        EchoFilter {}
    }
}

impl MessageFilter for EchoFilter {
    fn init(&mut self) {}

    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError> {
        match message.command {
            StompCommand::Message => {
                if !context.is_downstream() {
                    return Ok(CONTINUE);
                }

                // Should not receive MESSAGE input except from upstream
                match message.get_header("destination") {
                    Some(destination_name) => {
                        if destination_name.ends_with("/up") {
                            let mut reply_destination = destination_name.clone();
                            reply_destination.split_off(destination_name.len() - 3);

                            if let Ok(session) = context.session.try_write() {
                                let mut reply_message =
                                    message.clone(Ownership::Session, message.id);
                                reply_message.remove_header("destination");
                                reply_message.push_header(Header {
                                    name: String::from("destination"),
                                    value: reply_destination,
                                });
                                reply_message.push_header(Header {
                                    name: String::from("proxy-via"),
                                    value: SERVER_TAG.to_string(),
                                });
                                session.send_message(Arc::new(reply_message));
                            }
                        }
                    }
                    None => {
                        return Err(FilterError {
                            reply_message: some_string("syntax"),
                            log_message: some_string("missing destination header"),
                            fatal: false,
                        })
                    }
                }
            }
            _ => {}
        }

        Ok(CONTINUE)
    }
}
