pub mod ack;
pub mod admin;
pub mod closure;
pub mod connect;
pub mod disconnect;
pub mod echo;
pub mod http;
pub mod log;
pub mod persist;
pub mod ping;
pub mod receipt;
pub mod requires_auth;
pub mod resend;
pub mod send;
pub mod subscribe;

use crate::message::stomp_message::StompMessage;
use crate::workflow::context::Context;
use std::fmt::Debug;

#[derive(Debug)]
pub struct FilterError {
    /// Message sent to client in the ERROR response
    pub reply_message: Option<String>,
    /// Text logged by the server
    pub log_message: Option<String>,
    /// if true session will be shutdown
    pub fatal: bool,
}

/// Outer Filter for processing StompMessage that may be HTTP
pub trait MessageFilter: Debug {
    fn init(&mut self);

    /// handle a message, return true if the message was fully processed or false if the filter
    /// expects the chain to continue.
    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError>;
}

#[derive(Debug)]
pub struct HttpError {
    pub log_message: Option<String>,
}

/// Inner filters that only deal with HTTP messages
pub trait HttpFilter: Debug {
    fn init(&mut self);

    /// handle a message, return true if the message was fully processed or false if the filter
    /// expects the chain to continue.
    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, HttpError>;
}

/// Inner filters that only deal with /virt/admin/ STOMP messages
pub trait AdminFilter {
    fn init(&mut self);

    /// handle a message, return true if the message was fully processed or false if the filter
    /// expects the chain to continue.
    fn do_filter(
        &self,
        context: &mut Context,
        message: &mut StompMessage,
    ) -> Result<bool, FilterError>;
}
