//! Utility methods.

/// returns Some(String::from(s))
pub fn some_string(s: &'static str) -> Option<String> {
    Some(String::from(s))
}

static ROLLOVER_LOWER_LIMIT: usize = usize::max_value() / 4;
static ROLLOVER_UPPER_LIMIT: usize = usize::max_value() / 4 * 3;

/// returns true if new > old, if we rollover usize this still works with range (1 quarter of usize)
pub fn rollover_gt(new: usize, old: usize) -> bool {
    if new < ROLLOVER_LOWER_LIMIT && old > ROLLOVER_UPPER_LIMIT {
        return true;
    } else {
        return new > old;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_rollover_gt() {
        assert!(!rollover_gt(0, 0));
        assert!(!rollover_gt(1, 1));
        assert!(rollover_gt(1, 0));
        assert!(rollover_gt(2, 0));
        assert!(rollover_gt(2, 1));
        assert!(rollover_gt(0, usize::max_value() - 10));
        assert!(rollover_gt(0, usize::max_value()));
        assert!(rollover_gt(1, usize::max_value()));
    }
}
