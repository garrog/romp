//! Bootstrapping, i.e. initializing, a `romp` server.
//!
use std::sync::{Arc, RwLock};
use std::time::Duration;
use std::pin::Pin;

use core::future::{Future, ready};
use core::task::{Poll, Context};

use futures::stream::{StreamExt, TryStreamExt};
use futures::future::TryFutureExt;


use log::*;

use tokio::net::TcpListener;
use tokio::time::{Instant, interval_at};

use tokio_stream::wrappers::IntervalStream;

use crate::downstream::downstream_bootstrap;
use crate::init::CONFIG;
use crate::init::SERVER;
use crate::init::USER;
use crate::session::interruptible_interval::InterruptibleInterval;
use crate::session::reader::{ReadKiller, Reader};
use crate::session::stomp_session::StompSession;
use crate::workflow::destination::destination::Destination;

/// Initialises the `romp` server, this method should be called once
/// bespoke filters have been registered with the routers.
pub fn romp_bootstrap() -> BootstrapFuture {
    // referencing the CONFIG here ensures it lazy loads first
    let server_name = CONFIG.name();
    info!("starting tokio for: {}", server_name);

    BootstrapFuture {}
}

pub struct BootstrapFuture {}

impl Future for BootstrapFuture {
    type Output = Result<(), ()>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        // referencing the struct causes it to init
        USER.len();

        debug!("starting destination server");
        for (_name, destination) in SERVER.iter() {
            tokio::spawn(destination_wrapper(destination.clone()));

            let mut expiry;
            {
                expiry = destination.read().unwrap().expiry();
            }
            let dest_clone = destination.clone();

            // short expiry threads take up lots of CPU
            if expiry < 60000 {
                expiry = 60000;
            }

            // TODO interruptible
            let task = IntervalStream::new(interval_at(Instant::now(), Duration::from_millis(expiry)))
                // dont understand this "move" by default the unused arg is borrowed and this affects closure lifetime checking somehow
                .map(move |_| dest_clone.write().unwrap().timeout_messages())
                .try_for_each(|a| ready(Ok(a)))
                .map_err(|_| {});

            tokio::spawn(task);
        }

        // clock tick every 60 seconds
        let ticker = IntervalStream::new(interval_at(Instant::now(), Duration::from_millis(60000)))
            .map(|_| SERVER.tick(cx))
            .try_for_each(|a| ready(Ok(a)))
            .map_err(|_| {});

        tokio::spawn(ticker);

        // bootstrap outgoing TCP connections
        downstream_bootstrap();

        for socket_address in CONFIG.socket_addresses.iter() {
            // Bind the server's sockets.
            let address = socket_address.parse().unwrap();
            let listener = match Box::pin(TcpListener::bind(&address)).as_mut().poll(cx) {
		Poll::Ready(listener) => listener.expect("unable to bind TCP listener"),
		Poll::Pending => panic!(),
	    };

            info!("listening on: {}", socket_address);

            // Pull out a stream of sockets for incoming connections
            // TODO the type of server (ForEach<...>) is stupidly complicated, so much so that we can not pass it around
            // https://tokio.rs/docs/going-deeper/returning/ does not help
            let server = listener
                .incoming()
                .map_err(|e| warn!("accept failed = {:?}", e))
                .for_each(|sock| {
                    if SERVER.is_shutdown() {
                        return Err(());
                    } else {
                        debug!("connection");
                    }

                    let session = Arc::new(RwLock::new(StompSession::new()));

                    // Session timeouts
                    let timeout_session = session.clone();
                    let task = InterruptibleInterval::new(session.clone())
                        .try_for_each(move |_| ready(timeout_session.write().unwrap().timeout(cx)))
                        .map_err(|_| {});
                    tokio::spawn(task);

                    let mut s = session.write().unwrap();

                    // Spawn the future as a concurrent task.
                    let (reader, writer) = s.split(sock, session.clone());

                    //let mq = Arc::new(RwLock::new(Mq::new()));
                    //let mq_cpy = mq.clone();
                    let read_killer = Arc::new(RwLock::new(ReadKiller::new()));
                    let read_killer_cpy = read_killer.clone();

                    // TODO combine these to one spawn task so we guarantee that all read write and mq ops happen on the same thread
                    tokio::spawn(writer);
                    tokio::spawn(read_wrapper(read_killer, reader));
                    s.set_read_killer(read_killer_cpy);

                    Ok(())
                });

            tokio::spawn(server);
        }
        Poll::Ready(Err(()))
    }
}

/// wraps a reader and a way to stop the reader
pub(crate) struct ReadWrapper {
    read_killer: Arc<RwLock<ReadKiller>>,
    reader: Reader,
}
pub(crate) fn read_wrapper(read_killer: Arc<RwLock<ReadKiller>>, reader: Reader) -> ReadWrapper {
    ReadWrapper {
        read_killer,
        reader,
    }
}
impl Future for ReadWrapper {
    type Output = Result<(), ()>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        debug!("read wrapper polled");
        {
            match Future::poll(Pin::new(&mut *self.read_killer.write().unwrap()), cx) {
                Poll::Ready(Err(_)) => {
                    match Future::poll(Pin::new(&mut self.reader), cx) {
                        Poll::Ready(Err(_)) => {}
                        _ => debug!("read killer unexpected return"),
                    }
                    debug!("read wrapper closed");
                    return Poll::Ready(Err(()));
                }
                _ => {}
            }
        }

        match Future::poll(Pin::new(&mut self.reader), cx) {
            Poll::Ready(Ok(())) => Poll::Ready(Ok(())),
            Poll::Pending => Poll::Pending,
            Poll::Ready(Err(_)) => {
                debug!("read wrapper closed");
                return Poll::Ready(Err(()));
            }
        }
    }
}

struct DestinationFuture {
    destination: Arc<RwLock<Destination>>,
    first_poll: bool,
}
fn destination_wrapper(destination: Arc<RwLock<Destination>>) -> DestinationFuture {
    DestinationFuture {
        destination,
        first_poll: true,
    }
}
impl Future for DestinationFuture {
    type Output = Result<(), ()>;

    // If this does not return NotReady it never polls again??
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        debug!("destination future wrapper polled");
        if self.first_poll {
            self.first_poll = false;
            {
                let mut d = self.destination.write().unwrap();
                d.set_task(cx.waker().clone());
            }
        }

        match self.destination.write().unwrap().poll() {
            true => Poll::Pending,
            false => {
                debug!("destination future closed");
                Poll::Ready(Err(()))
            }
        }
    }
}
