use std::sync::{Arc, RwLock};
use std::time::Duration;
use std::pin::Pin;

use core::task::{Poll, Context};
use core::future::Future;

use log::*;

use futures::Stream;

use tokio::time::{Instant, Sleep, sleep_until};

use crate::session::stomp_session::StompSession;

const MIN_TIMEOUT: i64 = 15000;

/// A stream representing notifications at fixed interval.  This is clone of Tokio code with
/// an added thread safe handle that can ensure a clean shutdown.

//#[derive(Debug)]
pub struct InterruptibleInterval {
    /// Future that completes the next time the `Interval` yields a value.
    delay: Pin<Box<Sleep>>,

    /// session to check for interrupting
    session: Arc<RwLock<StompSession>>,
}

impl InterruptibleInterval {
    /// Create a new `Interval` yields every `session.next_timeout()`

    pub fn new(session: Arc<RwLock<StompSession>>) -> InterruptibleInterval {
        InterruptibleInterval::new_with_delay(sleep_until(Instant::now()), session)
    }

    pub(crate) fn new_with_delay(
        delay: Sleep,
        session: Arc<RwLock<StompSession>>,
    ) -> InterruptibleInterval {
        InterruptibleInterval { delay: Box::pin(delay), session }
    }
}

impl Stream for InterruptibleInterval {
    type Item = Result<Instant, tokio::time::error::Error>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        debug!("interruptible interval polled");

        // do the interrupt thing
        if self.session.read().unwrap().shutdown_pending() {
            return Poll::Ready(Some(Ok(Instant::now())));
        }

        // Wait for the delay to be done
        let _ = match self.delay.as_mut().poll(cx) {
            Poll::Ready(()) => {},
            Poll::Pending => return Poll::Pending,
        };

        // Get the `now` by looking at the `delay` deadline
        let now = self.delay.deadline();

        let mut next_timeout;
        {
            next_timeout = self.session.read().unwrap().next_timeout();
            if next_timeout < MIN_TIMEOUT {
                next_timeout = MIN_TIMEOUT;
            }
            debug!("next timeout is {}", next_timeout);
        }
        self.delay.as_mut()
            .reset(now + Duration::from_millis(next_timeout as u64));

        // Return the current instant
        Poll::Ready(Some(Ok(now).into()))
    }
}
