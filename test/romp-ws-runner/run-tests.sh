#!/bin/bash

cd $(dirname $0)
for js in $(ls -1 *.js)
do
  test "$1" == "-v" && echo testing $js
  jshint $js
  node $js || echo "failed: $js"
done
