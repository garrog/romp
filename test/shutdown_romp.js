#!/usr/bin/node

var sys = require('util');
var stomp = require('stomp');

/*
 * Connect as admin and send a shutdown message.
 */
var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'admin',
    passcode: 'password',
};

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.send({
        destination: '/virt/admin/shutdown',
        ack: 'client',
        body : ""
    });
});

client.on('error', function(error_frame) {
    console.error("Error");
    console.error(error_frame.body);
    process.exit(1);
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(2);
});
