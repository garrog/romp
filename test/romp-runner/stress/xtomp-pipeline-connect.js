var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

/*
 * This is explicitly forbidden, but should not crash, it should timeout fast
 */
stompRaw.on("connect", function() {
    stompRaw.write(
        "CONNECT\n" +
        "\n\0" + 

        "SUBSCRIBE\n" +
        "destination:memtop-a\n" +
        "receipt:oioi\n" +
        "\n\0"
    );
});

stompRaw.on("frame", function(frame) {
    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") ) {
        console.log("server reported error");
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        console.log("unexpected receipt");
        process.exit(1);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
