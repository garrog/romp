#!/usr/bin/env node
"use strict";

const stomp = require('stomp');

const num = process.argv[2];

const stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'publisher',
    passcode: 'passcode',
    "heart-beat": "120000,120000",
}

const client = new stomp.Stomp(stomp_args);

client.sent = 0;

/*
 * Publish messages repeatedly (every 100ms)
 */

var GROUPS = [];
GROUPS.push("zero");
GROUPS.push("one");
GROUPS.push("two");
GROUPS.push("three");
GROUPS.push("four");
GROUPS.push("five");
GROUPS.push("six");
GROUPS.push("seven");
GROUPS.push("eight");
GROUPS.push("nine");

function doSend() {
    client.send({
        destination: 'memtop-f',
        grp: GROUPS[Math.floor(Math.random() * 10)],
        body: "Testing\n" + new Date() + "\n123",
        receipt : 'send'
    });
}

client.on('connected', () => doSend() );

client.on('receipt', (id) => {
    client.sent++;
    setTimeout(doSend, 10);
    if ( client.sent % 100 == 0 ) console.log("sent " + client.sent);
});

client.on('error', (error_frame) => {
    console.log(error_frame.headers);
    client.disconnect();
    setTimeout(function () {
        process.exit(1);
    }, 1000);
});

client.connect();

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
