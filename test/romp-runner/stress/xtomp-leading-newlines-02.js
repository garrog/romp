

var StompRaw = require('stomp-raw').StompRaw;
var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\r\n\r\n\0");
    setTimeout(function() {
        stompRaw.write(
            "\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\nSUBSCRIBE\r\n" +
            "destination:memtop-a\r\n" +
            "receipt:sub\r\n" +
            "\r\n\0");
    }, 10);
});

stompRaw.on("frame", function(frame) {
    //console.log(frame);
    if ( frame.startsWith("CONNECTED") ) {
    }
    else if ( frame.startsWith("ERROR") && frame.indexOf("message:syntax") > 0 ) {
        process.exit(1);
    }
    else if ( frame.startsWith("RECEIPT") ) {
        process.exit(0);
    }
    else {
        process.exit(1);
    }
});

stompRaw.connect();
setTimeout(function() {
    stompRaw.end();
}, 100);
