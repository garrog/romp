#!/bin/bash

node stress/subscribe-alot.js &
node stress/publish-alot.js &
node stress/subscribe-hash-alot.js &
node stress/publish-hash-alot.js &

while true
do
	sleep 60
done

kill -9 %1 %2

