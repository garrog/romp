#!/bin/bash -e
#
# Test 50,000 subscribers and 5,000,000 messages
#
# xtomp performs quite well in this test at time of writing, 80 seconds odd to run the whole test.
# romp not bad 150 seconds.
#
if [[ $(id -u) != "0" ]]
then
	echo "run as root"
	exit 1
fi

cd $(dirname $0)

msg_count=100

sleep 1
node subscribe $msg_count &

echo sleep 10
sleep 10
node publish $msg_count &

wait
