"use strict";

const sys = require('util');
const stomp = require('stomp');

const stomp_args = {
    port: 61613,
    host: 'localhost',
    debug: false,
    login: 'guest',
    passcode: 'guest'
};

var msgCount = process.argv[2];
msgCount = msgCount || 100;

var messages = 0;
var connected = 0;
const CLIENTS = 50000;
const clients = [];
const start = new Date().getTime();

const createClient = function(id) {

    stomp_args.localAddress = "127.0.0." + (Math.floor(Math.random() * 20) + 1);
    var client = new stomp.Stomp(stomp_args);
    client.id = id;

    clients.push(client);

    client.on('connected', function() {
        //console.log('connected' + client.id);
        client.subscribe({
            destination: 'memtop-a',
            id : '1',
            ack: 'client',
            receipt: 'sub' 
        });
    });

    client.on('receipt', function(id) {
    //    console.log('receipt received:' + id);
        if (id === 'sub') {
            if ( ++connected === CLIENTS) {
                console.log('subscribed [clients="' + CLIENTS + '" d="' + (new Date().getTime() - start) + '"]');
            }
        }
    });

    client.on('message', function(message) {
        client.ack(message.headers['message-id']);
        if (++messages === (CLIENTS * msgCount)) {
            console.log('received [messages="' + messages + '" d="' + (new Date().getTime() - start) + '"]');
            cleanExit();
        }
    });

    client.on('error', function(error_frame) {
        console.log("error");
        console.log("headers: " + sys.inspect(error_frame.headers));
    //    console.log("body: " + error_frame.body);
        client.disconnect();
    });

    client.connect();

}

const cleanExit = function() {
    console.log('\nconsumed [messages="' + messages + '" d="' + (new Date().getTime() - start) + '"]');
    for (var i = 0 ; i < CLIENTS ; i++) {
        clients[i].disconnect();
    }
}

process.on('SIGINT', cleanExit);


for (var i = 0 ; i < CLIENTS ; i++ ) {
    createClient(i);
}



