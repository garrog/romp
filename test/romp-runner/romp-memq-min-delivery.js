const sys = require('util');
const stomp = require('stomp');

// seems to be significantly slower than xtomp
const TEST_TIMEOUT = 500;

/*
 * memq test that message is queued and delivered when the first subscriber connects.
 */
const stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: "memq",
    passcode: "passcode",
};

const destination = "memq-1";
const testMessage =  "Testing01\n";

const errorHandler = function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    process.exit(1);
};

stomp_args.login = "sub1";
const subscriber1 = new stomp.Stomp(stomp_args);
stomp_args.login = "sub2";
const subscriber2 = new stomp.Stomp(stomp_args);
stomp_args.login = "publisher";
const publisher = new stomp.Stomp(stomp_args);

// SUBSCRIBER1 - start

subscriber1.on("connected", function() {
    subscriber1.subscribe({
        destination: destination,
        id : "1",
        ack: "client",
        receipt: "sub1"
    });
});

subscriber1.on("receipt", function(id) {
    if (id === "sub1") {
    }
    else if (id === "unsub1") {
        subscriber1.disconnect();
        subscriber2.connect();
    }
    else console.error("sub1 unexpected receipt " + id);
});

subscriber1.on("message", function(message) {
    if (testMessage !== "" + message.body) {
        console.error("wrong msg");
        console.error("want:" + testMessage);
        console.error("got:" + message.body);
        errorHandler({});
    }
    else {
        subscriber1.ack(message.headers["message-id"]);
        subscriber1.unsubscribe({
            destination: destination,
            id : "1",
            receipt: "unsub1"
        });
    }
});

subscriber1.on("error", errorHandler);

// SUBSCRIBER1 - end

// SUBSCRIBER2 - start

subscriber2.on("connected", function() {
    subscriber2.subscribe({
        destination: destination,
        id : "2",
        ack: "client",
        receipt: "sub2"
    });
});

subscriber2.on("receipt", function(id) {
    if (id === "sub2") {
        setTimeout(function(){
            process.exit(0);
        }, 100);
    }
    else if (id === "unsub2") {
        subscriber2.disconnect();
    }
    else console.error("sub2 unexpected receipt");
});

subscriber2.on("message", function(message) {
    console.log("Error sub 2 got the message");
    process.exit(1);
});

subscriber2.on("error", errorHandler);

// SUBSCRIBER2 - end

// PUBLISHER - start

publisher.on("connected", function() {
    publisher.send({
        "destination" : destination,
        "body" : testMessage,
        "receipt" : "send"
    });
});

publisher.on("receipt", function(id) {
    if (id === "send") {
        publisher.disconnect();
        subscriber1.connect();
    }
    else {
        console.error("wrong receipt: " + id);
    }
});

publisher.on("error", errorHandler);

// PUBLISHER - end

process.on("SIGINT", function() {
    publisher.disconnect();
    subscriber1.disconnect();
    subscriber2.disconnect();
});

publisher.connect();

setTimeout(function() {
    console.log("test timeout");
    process.exit(1);
}, TEST_TIMEOUT);
