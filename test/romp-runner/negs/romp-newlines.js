var StompRaw = require('stomp-raw').StompRaw;

/*
 * Test extra \n usecase.
 */

var stompRaw = new StompRaw();

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    // console.log(frame);

    if ( frame.startsWith("CONNECTED") ) {
        setInterval(() => {

            stompRaw.write("\n");

        }, 50);

        setTimeout(() => {
            stompRaw.write("SEND\ndestination:memtop-a\n\nmessage data\0");
        }, 100);

        setTimeout(() => {
            stompRaw.end();
            process.exit(0);
        }, 200);

    }
    else {
        console.log("error");
        stompRaw.end();
    }
});

stompRaw.connect();
