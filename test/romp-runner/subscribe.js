var sys = require('util');
var stomp = require('stomp');

// subscribe to memtop-a and hang, on Ctrl+C prints count of messages consumed

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'sub1',
    passcode: 'passcode',
};

var client = new stomp.Stomp(stomp_args);

var messages = 0;

client.connect();


client.on('connected', function() {
    //console.log('connected');
    client.subscribe({
        destination: 'memtop-a',
        id : 23,
        ack: 'auto',
        foo: 'baa',
        receipt: 'sub' 
    });
});

client.on('receipt', function(id) {
    //console.log('receipt received:' + id);
});

client.on('message', function(message) {
    //console.log("headers: " + sys.inspect(message.headers));
    //console.log("body: " + message.body);
    client.ack(message.headers['message-id']);
    messages++;
});

client.on('error', function(error_frame) {
    console.log("error");
    console.log("headers: " + sys.inspect(error_frame.headers));
    console.log("body: " + error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    console.log('\nconsumed ' + messages + ' messages');
    client.disconnect();
});
