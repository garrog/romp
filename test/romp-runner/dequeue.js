#!/usr/bin/env node

// Manually pop messages from memq-1 ack and exit (not part of test suite)

var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'sub1',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.connect();

client.on('connected', function() {
    client.subscribe({
        destination: 'memq-1',
        id : '1',
        ack: 'client',
        receipt : 'sub'
    });
});

client.on('message', function(message) {
    console.log("popped: " + message.headers["message-id"]);
    client.ack(message.headers["message-id"]);
});

client.on('receipt', function(id) {
    // console.log("receipt received: " + id);
    if (id === 'sub') {
        setTimeout( () => {
            client.disconnect();
        }, 500);
    }
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});
