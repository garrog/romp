var StompRaw = require('stomp-raw').StompRaw;

/*
 * Use content length and whole packets
 */
var stompRaw = new StompRaw({
    debug : false
});

var to = setTimeout(function() {
    process.exit(1);
}, 60000);

stompRaw.on("connect", function() {
    stompRaw.write("CONNECT\nlogin:xtomp\npasscode:passcode\n\n\0");
});

stompRaw.on("frame", function(frame) {

    while (frame.charAt(0) === '\n' || frame.charAt(0) === '\r') {
        console.log("leading whitespace detected");
        frame = frame.substring(1);
    }

    //console.log("frame:" + frame);
    var howSlow = 1;

    if ( frame.startsWith("CONNECTED") ) {
        //console.log("connected");
        stompRaw.write("SEND\n" +
                       "destination:memtop-a\n");
        setTimeout(function(){
            stompRaw.write("receipt:send\n" +
                           "content-length:30\n" +
                           "\n");
        }, 100);
        setTimeout(function(){
            stompRaw.write("123456789\n" +
                           "123456789\n" +
                           "123455789\n\0");
        }, 200);
    }
    else if ( frame.trim().startsWith("RECEIPT") ) {
        stompRaw.write("DISCONNECT\n\n\0");
        clearTimeout(to);
    }
    else {
        console.log("unexpected frame");
        stompRaw.end();
    }
});

stompRaw.connect();
