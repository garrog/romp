#!/usr/bin/env node

// remove message from a queue, run with DEST=[queue name] node drain.js

var stomp = require('stomp');

var stomp_args = {
    port: 61613,
    host: process.argv[2] ? process.argv[2] : 'localhost',
    debug: false,
    login: 'xtomp',
    passcode: 'passcode',
}

var client = new stomp.Stomp(stomp_args);

client.on('connected', function() {
    client.messages = 0;
    client.subscribe({
        destination: process.env.DEST,
        id : '1'
    });
});

client.on('message', function(message) {
    client.messages++;
});

client.on('receipt', function(id) {
});

client.on('error', function(error_frame) {
    console.log(error_frame.body);
    client.disconnect();
});

process.on('SIGINT', function() {
    client.disconnect();
    process.exit(0);
});

client.connect();
setTimeout( () => {
    console.log("drained " + client.messages + " messages");
    client.disconnect();
    process.exit(0);
}, 500);