#!/bin/bash

cd $(dirname $0)

failed=false
function failed() {
    failed=true
    echo $*
}

date=$(date)
export TEST_DATE=${TEST_DATE:-$date}

for rompjs in $(ls -1 romp-*.js negs/romp-*.js stress/romp-*.js)
do
    if [[ "$1" == "-v" ]]
    then
        echo "$rompjs $2"
        jshint $rompjs
        node $rompjs $2 || failed "$rompjs failed"
        # echo '---'
    else
        jshint $rompjs
        node $rompjs $2 || failed "$rompjs failed"
    fi
done

[ "$failed" == "false" ] && exit 0
