![romp](doc/romp-rs-128.png)
# Romp a stomp server written in rust

Romp is an in memory messaging server, based on xtomp (a similar beast written in C).

Clients can connect with [STOMP protocol](https://stomp.github.io) over TCP, or STOMP over websockets.  

STOMP is to pub/sub what HTTP is to request and response.

Example STOMP frame

    CONNECT
    login:xtomp
    passcode:mypassword
    
    \0

There are a great many STOMP clients for most languages and platforms, writing a client from scratch is trivial.  
Its also pretty simple to connect to with a telnet client and type the protocol by hand.

Romp functions as a WebSockets development platform by allowing rust code to handle certain message types.
Romp also allows rust handlers for GET HTTP methods, (not POST). HTTP GET handling is needed to perform
the initial requests to Upgrade an HTTP connection to WebSockets, but a generic framework exists to allow arbitrary rust code
to handle requests before an Upgrade is made on the request.

## Standalone Usage

Romp can be run as a standalone STOMP server with `cargo run` by default `/etc/romp.roml` is read with the server configuration.
Romp opens the following ports
 
* 8080 - presumed to be web-facing for HTTP GET & web-socket connections.
* 61613 - standard STOMP port, presumed o be TCP connections, either web or local lan.
* 61616 - administration port, presumed to NOT be web facing, should be blocked in the firewall on cloud deployments, or set `enable_admin = false`

Clients can connect with dedicated STOMP clients, browsers with STOMP/WebSockets, or direct with telnet.

## Server side code

Romp can be loaded as a library from `crates.io`, at time of writing romp uses tokio 1.x, i.e. pre async.

Minimum server main for romp is as follows

    fn main() {
        tokio::run(romp_bootstrap());
    }

This starts the default server.

Bespoke HTTP GET and STOMP message handlers can be added to the configuration by calling init methods _before_ bootstrapping the server.  
Calling filter methods after `romp_bootstrap()` is unsafe.

Message handlers use the filter pattern, which should be familiar to Java Servlets developers.
Rather than I/O streams the filters are provided with the entire message pre-parsed and apis to send responses.  

Filters for handling STOMP messages should implement the `MessageFilter` trait, filters for HTTP GET requests should implement `HttpFilter`.

Both these traits provide an `init()` method to setup initial state of the filter that will be called once during server initialization
and a `do_filter(context: &mut Context, message: &StompMessage)` that is called for each request.

The two methods to insert filters are as follows

    romp_stomp_filter(Box::new(PingFilter {}));

and

    romp_http_filter(Box::new(HelloFilter {}));

`romp::workflow::filter::ping::PingFilter`, and `romp::workflow::filter::http::hello::HelloFilter` contain example code for handling messages. 

Two convenience methods are provided that accept closures instead of pointers to Filters.

    romp_stomp(|ctx, msg|)

and

    romp_http(|ctx, msg|);

Filter code should avoid blocking the thread, the `Context` struct contains a `StompSession` which contains methods for sending replies
asynchronously to the client.

## Build from source

N.B. Unix only, (uses libc) designed to be easy to deploy in containers, [lxinitd](https://lxinitd.tp23.org/), lcx, lxd or docker.

Source code: [gitlab.com/teknopaul](https://gitlab.com/teknopaul/romp)

Install rust with rustup

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

You may have to uninstall the package manager's version of rustc which is a bit crap but rustup folks have 
no intention of supporting any other package manger or even trying to co-exist with them.  Boo.

    apt remove rustc

Ensure cargo is in your path

    . go

Or add the following to your profile

    . $HOME/.cargo/env

Then run `make` or `cargo build`

To build a statically bound binary

	rustup target add x86_64-unknown-linux-musl
	cargo build --release --target=x86_64-unknown-linux-musl

## BUGs

N.B bugs and features moved to .later

## References

- [stomp](https://stomp.github.io)
- [rust up](https://rustup.rs/)
- [the book](https://doc.rust-lang.org/stable/book/)
- [tokio](tokio.rs)
- [serde](https://serde.rs/)
- [log4rs](https://github.com/estk/log4rs)
